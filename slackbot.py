"""Slack API intergation."""

import time
from slackclient import SlackClient
from slither_sloth import *
from bot_id import *


BOT_ID = find_my_bot()
AT_BOT = "<@" + BOT_ID + ">"
BOT_COMMAND = "news"


def handle_command(command, channel):
    response = "Hmm, I don't understand much yet, use the *" + BOT_COMMAND + \
               "* command if you need something from me."
    if command.startswith(BOT_COMMAND):
        response = news_list()
    slack_client.api_call("chat.postMessage", channel=channel,
                          text=response, as_user=True)


def parse_slack_output(slack_rtm_output):
    output_list = slack_rtm_output
    if output_list and len(output_list) > 0:
        for output in output_list:
            if output and 'text' in output and AT_BOT in output['text']:
                return output['text'].split(AT_BOT)[1].strip().lower(), \
                       output['channel']
    return None, None


if __name__ == "__main__":
    READ_WEBSOCKET_DELAY = 1
    if slack_client.rtm_connect():
        print("Slackbot connected and running!")
        while True:
            command, channel = parse_slack_output(slack_client.rtm_read())
            if command and channel:
                handle_command(command, channel)
            time.sleep(READ_WEBSOCKET_DELAY)
    else:
        print("Connection failed. Invalid Slack token or bot ID?")
