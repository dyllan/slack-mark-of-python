"""
Using Codeacademy's Markov Chain method to pull news titles from
Reddit and feed them to the Slack API. A slack bot can then be interacted with
via Slack and it's responses will be a markov of the reddit news titles.
"""

import math, re, urllib2
from markov_python.cc_markov import MarkovChain
from bs4 import BeautifulSoup


def scrape_web():
    # url to scrape
    url = "http://www.reddit.com/"
    hdr = { 'User-Agent' : 'just a friendly coder slash bot' }
    req = urllib2.Request(url, headers=hdr)
    content = urllib2.urlopen(req).read()
    soup = BeautifulSoup(content, "html.parser")
    news_file = open("red_news_titles.txt", "r+")
    for x in soup.find_all('p')[15:70:3]:
        x = x.text.encode('utf-8')
        x = re.sub("[\(\[].*?[\)\]]", "", x)
        news_file.write(x)
        news_file.write("\n")
    news_file.close()


def news_list():
    scrape_web()
    mc = MarkovChain()
    mc.add_file("red_news_titles.txt")
    news = ' '.join(mc.generate_text(20))
    slither_sloth = "%s." % news.capitalize()
    return slither_sloth
