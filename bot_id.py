"""Get Slack bot ID"""

from slackclient import SlackClient

# The name of your custom slack bot
BOT_NAME = "bossbot"
# The token number for your custom slack bot
slack_client = SlackClient("")


def find_my_bot():
    api_call = slack_client.api_call("users.list")
    if api_call.get('ok'):
        users = api_call.get("members")
        for user in users:
            if "name" in user and user.get("name") == BOT_NAME:
                BOT_ID = user.get("id")
                return BOT_ID
    else:
        print("could not find bot user with the name " + BOT_NAME)
