# slack-mark-of-python
Final Codeacademy python project

This is a final python project for the Codeacademy python course.
It makes use of the CA markov chain method to fetch data from http://www.reddit.com/ using urllib2. The data scraped are the
'news' titles which are then formatted for use in our slackbot module. The slackbot code interacts with the Slack API and a 
custom bot we create via Slack by mentioning the bot from within Slack and using a trigger command, in this case 'news', to
trigger the bot which then responds with the markov chain data we created which is basically a chain of all the headings from
Reddit news at that time. Every time the bot is mentioned with the trigger the markov chain code is called an a new response
is returned.

In order for this to work the program needs to run and listen on the command line. It connects to the Slack client API and 
check for new mentions/triggers every second and responds accordingly.
I used most of Matt Makai's code to get the Slack intergration working, it can be found here: 
https://www.fullstackpython.com/blog/build-first-slack-bot-python.html
